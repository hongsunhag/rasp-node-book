const config = require('./config')
const onoff = require('onoff')
const led = require('./led')

const led1 = new onoff.Gpio(config.led[0], 'out')

// led.check(led1)
led.blink(led1)