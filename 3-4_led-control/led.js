function lightOnce (led) {
	led.write(1, (error) => {
		setTimeout(() => {
			led.write(0, (error) => {			
			})
		}, 1000)	
	})
}

exports.check = (led) => {
	lightOnce(led)
}

exports.blink = (led) => {
	setInterval(() => {
		lightOnce(led)
	}, 2000)
}