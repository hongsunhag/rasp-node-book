let now = exports.now = () => {
	now = new Date()
    let time1 = now.toTimeString()
	let date1 = now.toDateString()

	return { date: date1, time: time1.slice(0, 8) }
}

console.log(now())