const LCD = require('lcdi2c')
const config = require('./config')
const time = require('./time')

const lcd1 = new LCD(config.busID, Number(config.i2cAddress),
					 config.matrix.width, config.matrix.height)

const clock = (lcd) => {
	return () => {
		const now = time.now()

		lcd.println(now.date, 1)
		lcd.println(now.time, 2)
	}
}

lcd1.clear()
setInterval(clock(lcd1), 1000)

console.log('Date Time Clock Executed')

process.on("SIGINT", () => {
	console.log('')
	console.log('...')
	console.log('Caught SIGINT. Exiting...')

	lcd1.clear()
	setTimeout(() => {
		lcd1.off()
		process.exit(0)
	}, 500)
})