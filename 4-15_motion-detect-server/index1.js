const fs = require('fs')
const http = require('http')
const router = require('tiny-router')
const webSocket = require('ws')
const onoff = require('onoff')
const config = require('./config')
const servicePort = 8000

router.get('/', (req, res) => {
	fs.readFile('index.html', (error, data) => {
		res.send(data.toString())
	})
})

const server = http.createServer(router.Router())
const wss = new webSocket.Server({ server })
const button = new onoff.Gpio(config.button[0], 'in', 'both', { debounceTimeout: 10 })

wss.on('connection', (ws) => {
	ws.send('hello browser')

	ws.on('message', (message) => {
		console.log('received:', message)
	})

	button.watch((error, value) => {
		console.log('button status:', value)
		ws.send(JSON.stringify({ msg: 'button status changed', status: value }))
	})
})

server.listen(servicePort, () => {
	console.log('web server listening on port', servicePort)
})