const config = require('./config')

exports.init = (sqlite) => {
	const db = new sqlite(config.db.name)
	const query = `CREATE TABLE IF NOT EXISTS pir (
		UID   		INTEGER PRIMARY KEY,
		TYPE  		TEXT    NOT NULL,
		CREATED_AT  TEXT
	);
	DROP INDEX IF EXISTS idx_pir_uid;
	CREATE INDEX idx_pir_uid ON pir (uid);`
	db.exec(query)

	return db
}