const sqlite = require('better-sqlite3')
const db = new sqlite('motion-detected.sqlite')

let query = `
CREATE TABLE IF NOT EXISTS PIR (
    UID   		INTEGER PRIMARY KEY,
    TYPE  		TEXT    NOT NULL,
	CREATED_AT  TEXT
);
DROP INDEX IF EXISTS idx_pir_uid;
CREATE INDEX idx_pir_uid ON pir (uid);
`
db.exec(query)

let statement = db.prepare(`INSERT INTO pir VALUES (?, ?, DATETIME('NOW', 'LOCALTIME'))`)
statement.run(new Date().getTime(), 'detected')
statement.run(new Date().getTime(), 'released')

let rows = db.prepare('SELECT * FROM pir LIMIT ?').all(10)
console.info(rows, 'is array type?', Array.isArray(rows))

let row = db.prepare('SELECT * FROM pir ORDER BY uid DESC LIMIT ?').get(1)
console.log(row, 'is array type?', Array.isArray(row))

let result = db.prepare('UPDATE pir SET type = :type WHERE uid = :uid')
			   .run({ type: 'expired', uid: row['UID'] })
console.log(result, 'updated result')
let updatedRow = db.prepare('SELECT * FROM pir WHERE uid = ?').get(result.lastInsertROWID)
console.log(updatedRow, 'updated row')

let deleted = db.prepare('DELETE FROM pir WHERE uid = ?').run(updatedRow['UID'])
console.log(deleted, 'deleted result')
let rowsNow = db.prepare('SELECT * FROM pir LIMIT ?').all(10)
console.info(rowsNow, 'remained rows')

db.exec('DROP INDEX IF EXISTS idx_pir_uid')
db.exec('DROP TABLE pir')