const fs = require('fs')
const http = require('http')
const onoff = require('onoff')
const sqlite = require('better-sqlite3')
const router = require('tiny-router')
const mustache = require('mustache')
const config = require('./config')
const db = require('./db')

const connection = db.init(sqlite)

router.use('engine', { eng: 'mustache', ext: 'html' })
router.use('view', __dirname)
router.get('/', (req, res) => {
	let rows = connection.prepare('SELECT * FROM pir ORDER BY uid DESC LIMIT ?').all(25)

	res.render('index', { list: rows })
})

const motion = new onoff.Gpio(config.pir[0], 'in', 'both', { debounceTimeout: 1000 })
motion.watch((error, value) => {
	let statement = connection.prepare(`INSERT INTO pir VALUES (?, ?, DATETIME('NOW', 'LOCALTIME'))`)

	if (value == 1) {
		statement.run(new Date().getTime(), 'detected')
	} else {
		statement.run(new Date().getTime(), 'released')
	}
})

const server = http.createServer(router.Router())
server.listen(config.servicePort, () => {
	console.log('web server listening on port', config.servicePort)
})