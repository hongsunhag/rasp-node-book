const fs = require('fs')
const http = require('http')
const servicePort = 8000

const handler = (req, res) => {
	fs.readFile('index.html', (error, data) => {
		if (error) {
			res.writeHead(404, { 'Content-Type': 'text/html' })
			res.end("File Not Found")
		} else {
			res.writeHead(200, { 'Content-Type': 'text/html' })
			res.write(data)
		}
	})
}

const server = http.createServer(handler)

server.listen(servicePort, () => {
	console.log('web server listening on port', servicePort)
})