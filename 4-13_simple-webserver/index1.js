const http = require('http')
const servicePort = 8000

const handler = (req, res) => {
	let host = req.headers['host']
	let client = req.connection.remoteAddress

	res.writeHead(200, { 'Content-Type': 'text/plain' })
	res.end(`Hello you are coming to ${host} from ${client}`)
}

const server = http.createServer(handler)

server.listen(servicePort, () => {
	console.log('web server listening on port', servicePort)
})