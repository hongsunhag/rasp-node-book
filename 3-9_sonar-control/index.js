const config = require('./config')

const pi = require('pigpio')
const trigger1 = new pi.Gpio(config.sonarTrig[0], { mode: pi.Gpio.OUTPUT })
const echo1 = new pi.Gpio(config.sonarEcho[0], { mode: pi.Gpio.INPUT, alert: true })

const sonarSpeed = 34321
const msDivider = 1000000 / sonarSpeed

let start = 0
let end = 0

const startTrigger = () => {
	trigger1.trigger(config.triggerTime, 1)
}

const echoListener = (level, tick) => {
	if (level == 1) {
		start = tick
	} else {
		end = tick
		let diff = (end >> 0) - (start >> 0)	// Unsigned 32 bit arithmetic
		let distance = (diff / 2) / msDivider

		console.log(`${Math.floor(distance)} cm`)

		setTimeout(startTrigger, 1000)
	}
}

trigger1.digitalWrite(0)
echo1.on('alert', echoListener)

startTrigger()
