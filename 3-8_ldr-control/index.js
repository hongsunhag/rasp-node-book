const config = require('./config')

const piGpio = require('pigpio')
const nanoTimer = require('nanotimer')

const ldr1 = new piGpio.Gpio(config.ldr[0])
const timer1 = new nanoTimer()

const densityReader = () => {
	ldr1.mode(piGpio.Gpio.INPUT)

	const checker = () => {
		while (ldr1.digitalRead() == 0) {}
	}

	let micro = timer1.time(checker, '', 'u')

	console.log('light density:', micro)

	setTimeout(getLightDensity, config.retryInterval)
}

const signalTrigger = (callback) => {
	ldr1.mode(piGpio.Gpio.OUTPUT)
	ldr1.digitalWrite(0)

	timer1.setTimeout(callback, '', config.waitTime)
}

const getLightDensity = () => {
	signalTrigger(densityReader)
}

getLightDensity()