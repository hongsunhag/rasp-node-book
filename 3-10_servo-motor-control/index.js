const config = require('./config')
const misc = require('./misc')
const pi = require('pigpio')

const arg1 = Number(process.argv[2])
const arg2 = Number(process.argv[3])
const motor1 = new pi.Gpio(config.servo[0], { mode: pi.Gpio.OUTPUT })

const timerInterval = 1000
let timeoutCounter = arg2 || 5

const isValid = value => Number.isInteger(value) && value >= 500 && value <= 2500
const randomPulse = () => Math.floor(Math.random() * 2000) + 500
const moveMotor = (motor, pulseWidth) => () => {
	if (!pulseWidth) {
		pulseWidth = randomPulse()
	}

	console.log('pulse with is', pulseWidth)
	motor.servoWrite(pulseWidth)

	if (timeoutCounter > 1) setTimeout(moveMotor(motor), timerInterval)
	timeoutCounter -= 1
}

if (arg1 && isValid(arg1)) {
	setTimeout(moveMotor(motor1, arg1), 100)
} else {
	misc.helpMessage()
}
