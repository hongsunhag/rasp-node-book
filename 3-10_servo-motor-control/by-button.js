const config = require('./config')
const pi = require('pigpio')

const led1 = new pi.Gpio(config.led[0], { mode: pi.Gpio.OUTPUT })
const motor1 = new pi.Gpio(config.servo[0], { mode: pi.Gpio.OUTPUT })
const button1 = new pi.Gpio(config.button[0], { mode: pi.Gpio.INPUT,
												pullUpDown: pi.Gpio.PUD_DOWN,
												edge: pi.Gpio.EITHER_EDGE,
												alert: true })

const moveMotor = pulseWidth => () => motor1.servoWrite(pulseWidth)
const getAngle = value => value ? 2500 : 500
const debounceLimit = 100000	// 100ms

button1.glitchFilter(debounceLimit)
button1.on('alert', (level, tick) => {
	console.log('button pressed:', level)

	if (level) {
		let ledStatus = led1.digitalRead()
		setImmediate(moveMotor(getAngle(ledStatus)))
		led1.digitalWrite(ledStatus ^ 1)
	}
})

console.log(`Ready servo motor with button action`)