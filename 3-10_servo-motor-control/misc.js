const clc = require('cli-color')

exports.helpMessage = () => {
	console.log(`Set a pulse width for servo motor. 500 ~ 2500 is a valid value`)
	console.log(`next, Set a exit timeout value. a default value is 5 sec`)
	console.log(`----------------`)
	console.log(`$ npm start ${clc.green('1500')} ${clc.red('3')}`)
	console.log(`----------------`)
	console.log(`This time just make a ${clc.red('random')} number for the pulse width`)
}