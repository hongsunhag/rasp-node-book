const config = require('./config')
const log = require('./log')
const onoff = require('onoff')

const motion = new onoff.Gpio(config.pir[0], 'in', 'both', { debounceTimeout: 1000 })

motion.watch((error, value) => {
	if (!error) {
		if (value === 1) {
			log.detect(value)
		} else {
			log.release(value)
		}
	} else {
		throw Error(error)
	}
})
