const config = require('./config')
const fs = require('fs')
const path = require('path')
const clc = require('cli-color')

const readFileOption = {
	encoding: 'utf8', flag: 'r'
}

fs.readFile(path.join(__dirname, config.logFile), readFileOption, (err, data) => {
	if (!err) {
		const count = data.split('\n').length

		if (count > 0) {
			console.log('issued record count:', count)

			data.split('\n').filter((value) => {
				return value
			}).reverse().map((value, index) => {
				try {
					let log = JSON.parse(value)
					let time = new Date(log.time)

					console.log('issued:', `#${index} ${time.getMonth() + 1}/${time.getDate()}`, `${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`)
					console.log(log.message)
				} catch (e) {
					throw new Error(e)
				}
			})
		}
	}
})
