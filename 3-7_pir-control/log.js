const config = require('./config')
const fs = require('fs')
const path = require('path')
const clc = require('cli-color')

const writeFileOption = {
	encoding: 'utf8', flag: 'a'
}

const detect = (value) => {
	let data = {
		time: new Date(),
		message: 'motion detected',
		value: value
	}

	fs.writeFile(path.join(__dirname, config.logFile), JSON.stringify(data) + '\n', writeFileOption, (err) => {
		console.log('motion', clc.red('detected'))
	})
}

const release = (value) => {
	let data = {
		time: new Date(),
		message: 'sensor released',
		value: value
	}

	fs.writeFile(path.join(__dirname, config.logFile), JSON.stringify(data) + '\n', writeFileOption, (err) => {
		console.log('sensor', clc.green('released'))
	})
}

module.exports = {
	detect,
	release
}