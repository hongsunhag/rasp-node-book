const config = require('./config')
const dhtSensor = require('rpi-dht-sensor')
const clc = require("cli-color")

const option = process.argv[2]
const dht11 = new dhtSensor.DHT11(config.dht11[0])

const displayOnce = () => {
	let current = dht11.read()
	let now = new Date()

	console.log(clc.cyanBright('current:'), now.getHours(), '시', now.getMinutes(), '분', now.getSeconds(), '초')
	console.log("=", clc.yellow("humidity"), current.humidity)
	console.log("=", clc.magentaBright("temperature"), current.temperature)
}

const displayLoop = (timeout) => {
	try {
		displayOnce()
		setTimeout(() => {
			displayLoop(timeout)
		}, timeout * 1000)
	} catch (error) {
		throw new Error(error)
	}
}

if (option && Number(option)) {
	displayLoop(Number(option))
} else {
	console.log(`Set interval second for display Temperature & Humidity`)
	console.log(`--------------`)
	console.log(`$ npm start ${clc.green('10')}`)
	console.log(`--------------`)
	console.log('')

	displayOnce()
}
