const config = require('./config')
const onoff = require('onoff')

const led1 = new onoff.Gpio(config.led[0], 'out')

exports.on = (callback) => {
	led1.writeSync(1)

	callback()
}

exports.off = (callback) => {
	led1.writeSync(0)

	callback()
}