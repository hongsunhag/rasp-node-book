const led = require('./led')
const fs = require('fs')
const http = require('http')
const router = require('tiny-router')
const servicePort = 8000

router.get('/', (req, res) => {
	fs.readFile('index.html', (error, data) => {
		res.send(data.toString())
	})
})
router.get('/led/on', (req, res) => {
	led.on(() => {
		res.send({ success: true, status: 'on' })
	})
})
router.get('/led/off', (req, res) => {
	led.off(() => {
		res.send({ success: true, status: 'off' })
	})
})

const server = http.createServer(router.Router())

server.listen(servicePort, () => {
	console.log('web server listening on port', servicePort)
})