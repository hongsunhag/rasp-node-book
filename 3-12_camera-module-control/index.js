const config = require('./config')
const childProcess = require('child_process')
const terminalImage = require('terminal-image')

const now = new Date()
const today = `${now.getFullYear()}-${(now.getMonth() + 1)}-${now.getDate()}`
const fileName = `face-${today}.jpg`

let count = config.countdown

console.log(`taking a photo! in ${count} second`)

const countdown = (callback, next) => {
	console.log(`...${count}`)
	return () => {
		count = count - 1

		if (count > 0) {
			setTimeout(countdown(callback, next), 1000)
		} else {
			callback(next)
		}
	}
}

const takePhoto = (callback) => {
	childProcess.execFile(config.program, [...config.options, fileName], (error) => {
		if (error) {
			throw error
		}

		console.log('capturing done... now previewing...', fileName)

		callback(fileName)
	})
}

const imageView = (fileName) => {
	terminalImage.file(fileName).then((asciiImage) => {
		console.log(asciiImage)
	})
}

const camera = countdown(takePhoto, imageView)

camera()