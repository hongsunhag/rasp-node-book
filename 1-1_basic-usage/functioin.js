const say = function (message, value) {
	console.log(`message: ${message} => value: ${value}`)
}

for (let index = 0; index < 5; index++) {
	let sum = summing(index)
	let multi = multiplying(index)

	if (multi > sum) {
		say("곱이 더 크다", multi)
	} else {
		say("합이 더 크다", sum)
	}
	
	console.log('index:', index, `sum = ${sum}, multi = ${multi}`)	
}

function multiplying(val) {
	return val * val
}

function summing(val) {
	return val + val
}
