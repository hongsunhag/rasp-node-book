const SEC = 1000

const sayOne = () => { console.log('I m first one') }

const sayNext = () => { console.log('Say Next') }

const smallTalkForever = (message) => {
	let talk = `I am talking: ${message}`

	return () => { 
		console.log(talk)
	}
}

sayOne()

setTimeout(sayNext, 2 * SEC)

setInterval(smallTalkForever('하하'), 5 * SEC)