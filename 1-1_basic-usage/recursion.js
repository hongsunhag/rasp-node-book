const SEC = 1000

const smallTalkForever = (message, interval) => {
	let talk = `I am talking: ${message}`

	console.log(talk)

	setTimeout(function () {
		smallTalkForever(message, interval)
	}, interval)
}

smallTalkForever('호호', 5 * SEC)