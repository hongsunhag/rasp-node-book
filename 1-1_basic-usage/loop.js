const loopCount = 10

for (let index = 0; index < loopCount; index++) {
	let sum = index + index
	let multi = index * index

	if (multi > sum) {
		console.log("곱이 더 크다")
	} else {
		console.log("합이 더 크다")
	}
	
	console.log('index:', index, `sum = ${sum}, multi = ${multi}`)	
}