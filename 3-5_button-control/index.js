const config = require('./config')
const onoff = require('onoff')

const led1 = new onoff.Gpio(config.led[0], 'out')
const button = new onoff.Gpio(config.button[0], 'in', 'rising', { debounceTimeout: 10 })

button.watch((error, value) => {
	console.log('button pressed')
	led1.read((error, ledStatus) => {
		console.log('old led status:', ledStatus)

		led1.write(ledStatus ^ 1, (error) => {
			led1.read((error, newStat) => {
				console.log('new led status:', newStat)
			})
		})
	})
})