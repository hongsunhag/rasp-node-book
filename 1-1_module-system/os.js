const os = require('os')

let cpu = `${os.arch()} 시스템이며 ${os.cpus().length} 코어의 CPU 를 가지고 있습니다.`
let home = `사용자의 홈 디렉토리는 ${os.homedir()} 입니다.`

console.log('System Info:', cpu)
console.log('Home Info:', home)
