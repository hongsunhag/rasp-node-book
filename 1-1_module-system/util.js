const util = require('util')

const floatValue = 10.5
const someObject = {msg: 'hello', id: 1}

let str1 = util.format('%s:%s - %f => %i', 'Float', 'Integer', floatValue, floatValue)

console.log(str1)
console.log('JSON format:', util.format('%j', someObject))
console.log('Object format:', util.format('%o', someObject))
